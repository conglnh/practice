var express = require('express');
var mongodb = require('mongodb').MongoClient;
var bookRouter = express.Router();

var sql = require('mssql');

var router = function (nav) {
    var book = [
        {title: 'ABC', Genre: 'ABC', Author: 'ABC', Read: 'ABC'},
        {title: 'ABC', Genre: 'ABC', Author: 'ABC', Read: 'ABC'},
        {title: 'ABC', Genre: 'ABC', Author: 'ABC', Read: 'ABC'},
        {title: 'ABC', Genre: 'ABC', Author: 'ABC', Read: 'ABC'},
    ];
    bookRouter.route('/')
        .get(function (req, res) {
            var url = 'mongodb://localhost:27017/Practice';
            mongodb.connect(url ,function (err, db) {
                var collection = db.collection('books');
                collection.find({}).toArray(
                    function(err, results) {
                            res.render('bookListView', {
                                title: 'Hello from render handlebars',
                                nav: [{Link: '/Books', Text: 'Book'},
                                    {Link: '/Author', Text: 'Author'}],
                                books: results,
                            });
                        }
                );
            });
            // var request = new sql.Request();
            // request.query('select * from Books', function (err, recordset) {
            //     res.render('bookListView', {
            //         title: 'Hello from render handlebars',
            //         nav: [{Link: '/Books', Text: 'Book'},
            //             {Link: '/Author', Text: 'Author'}],
            //         books: recordset,
            //     });
            // });
        });

    bookRouter.route('/:id')
        .all(function (req, res, next) {
            var id = req.params.id;
            var ps = new sql.PreparedStatement();
            ps.input('id',sql.Int);
            ps.prepare('select * from books where Id = @id', function (err) {
                ps.execute({id:req.params.id}, function (err, recordset) {
                    if (recordset.length === 0)
                    {
                        res.status(404).send('Not found');
                    }
                    else {
                        req.book = recordset[0];
                        next();
                    }
                });
            });
        })
        .get(function (req, res) {
                        res.render('bookView', {
                            title: 'Hello from render handlebars',
                            nav: nav,
                            book: req.book,
                        });
                    });
    return bookRouter;
};

module.exports = router;