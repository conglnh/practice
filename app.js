var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var passport = require('passport');
var session = require('express-session');

var handlebars = require('express-handlebars');
var sql = require('mssql');

var config = {
    user: 'sa',
    password : '123456',
    server :'localhost',
    database : 'Books',
};
sql.connect(config , function (error) {
    if (error != null) {
        console.log(error);
    }
});
var nav = [{Link: '/Books', Text: 'Book'},
                {Link: '/Author', Text: 'Author'}];
var port = 8080 || 8080;
var bookRouter = require('./routes/bookRoutes')(nav);
var adminRouter = require('./routes/adminRoutes')(nav);
var authRouter = require('./routes/authRoutes')(nav);
require('./src/config/passport')(app);

app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(session({
    secret : 'Practice',
    resave : false,
    saveUninitialized : true
}));
app.use(passport.initialize());
app.use(passport.session());

app.set('views', './src/views');
app.engine('.hbs', handlebars({extname: '.hbs'}));
app.set('view engine', 'ejs');

app.use('/Books', bookRouter);
app.use('/Admin', adminRouter);
app.use('/auth', authRouter);

app.get('/', function (req, res) {
    res.render('index',
        {
            title: 'Hello from render handlebars',
            nav: [{Link: '/Books', Text: 'Book'},
                {Link: '/Author', Text: 'Author'}]
        });
});

app.listen(port, function (err) {
    console.log('server running on port ' + port);
});